#!/usr/bin/python3
# encoding: utf-8
"""A setuptools based setup module for adjust_price_http"""

from codecs import open
from os import path
from setuptools import setup, find_packages

import versioneer

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as readme_file:
    readme = readme_file.read()

with open(path.join(here, 'requirements.txt'), encoding='utf-8') as requirements_file:
    requirements = requirements_file.read()

with open(path.join(here, 'requirements_dev.txt'), encoding='utf-8') as requirements_dev_file:
    requirements_dev = requirements_dev_file.read()

ext_modules = []
setup(
    name='adjust_price_http',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="listing调价, 接收PHP推送调价列表的消息服务",
    long_description=readme,
    author="asinking",
    author_email='asinking@asinking.com',
    url='https://github.com/asinking/adjust-price-http',
    packages=find_packages(include=['adjust_price_http', 'adjust_price_http.*']),
    entry_points={
        'console_scripts': [
            'adjust_price_http=adjust_price_http.__main__:entry_point',
        ],
    },
    include_package_data=True,
    install_requires=requirements,
    python_requires=">=3.6",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    extras_require={'dev_require': requirements + "\n" + requirements_dev},
    ext_modules=ext_modules,
    keywords='adjust_price_http',
)
