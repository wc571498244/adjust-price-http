from typing import List, Dict, Optional

from pydantic import BaseModel, Field
from sqlalchemy import Column, String, Integer, TEXT, BIGINT, TIME

from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base


class Data(BaseModel):
    company_id: str
    zid: Optional[str]
    sid: Optional[str]
    msku: Optional[str]
    asin: Optional[str]
    rule_type: int = Field(..., gt=0, le=2, description="rule_type 必须是1或2！")
    mid: Optional[str]
    seller_id: Optional[str]
    rule_unique_id: Optional[int]
    site_url: Optional[str]
    status: Optional[int]
    is_delete: Optional[int]
    exec_start_time: Optional[str]
    exec_end_time: Optional[str]
    adjust_price_range: Optional[list]
    timezone: Optional[str]
    is_site_time: Optional[int]


class MessageContent(BaseModel):
    # 1新增2修改3删除4竞品规则range修改adjust_price_range
    operate_type: int = Field(..., gt=0, le=4, description="operate_type 必须在[1, 2, 3, 4]之间！")
    company_id: int
    data: List[Data]


class MessageList(BaseModel):
    messageId: Optional[str]
    messageRouterKey: Optional[str]
    messageTypeCode: Optional[str]
    messageContent: MessageContent
    createTime: Optional[int]
    mqInfo: Optional[Dict]
    mark: Optional[str]


class Msg(BaseModel):
    consumer: Optional[str]
    host: Optional[str]
    messageList: List[MessageList]


"""------------------------------------- ORM ---------------------------------------"""
Base: DeclarativeMeta = declarative_base()


class TableFollowSale(Base):
    __tablename__ = "follow_sale_adjust_price_scheduler"
    id = Column(Integer, primary_key=True)      # 主键
    zid = Column(Integer)                       # 企业ID
    company_id = Column(BIGINT)                 # 企业id
    asin = Column(String(16))                   # 亚马逊商品唯一编号
    sid = Column(Integer)                       # 店铺id
    mid = Column(Integer)                       # 地区id
    site_url = Column(String(30))               # 当前国家域名：https://www.amazon.com
    seller_id = Column(String(30))              # 亚马逊卖家编号（也叫MERCHANT_ID）
    rule_unique_id = Column(BIGINT)             # 调价规则id
    status = Column(Integer)                    # 开启状态，1：暂停、2：暂停且不可操作、3：开启
    is_delete = Column(Integer)                 # 是否删除，0未删，1已删
    version = Column(Integer)                   # 执行版本号
    exec_start_time = Column(TIME)              # 执行时间段开始时间
    exec_end_time = Column(TIME)                # 执行时间段结束时间
    seller_push_time = Column(Integer)          # 跟卖数据上次发布时间
    seller_update_time = Column(Integer)        # 跟卖数据上次更新时间
    msku = Column(String(30))                   # msku
    timezone = Column(String(20))               # 时区
    is_site_time = Column(Integer)              # 是否是站点时间，0：不是站点时间（北京时间），1：站点时间


class TableCompetitive(Base):
    __tablename__ = "competitive_adjust_price_scheduler"
    id = Column(Integer, primary_key=True)      # 主键
    zid = Column(Integer)                       # 企业ID
    company_id = Column(BIGINT)                 # 企业id
    asin = Column(String(16))                   # 亚马逊商品唯一编号
    sid = Column(Integer)                       # 店铺id
    mid = Column(Integer)                       # 地区id
    site_url = Column(String(30))               # 当前国家域名：https://www.amazon.com
    seller_id = Column(String(30))              # 亚马逊卖家编号（也叫MERCHANT_ID）
    rule_unique_id = Column(BIGINT)             # 调价规则id
    adjust_price_range = Column(TEXT)           # 需要调价的列表ASIN
    status = Column(Integer)                    # 开启状态，1：暂停、2：暂停且不可操作、3：开启
    is_delete = Column(Integer)                 # 是否删除，0未删，1已删
    version = Column(Integer)                   # 执行版本号
    exec_start_time = Column(TIME)              # 执行时间段开始时间-格林威治时间
    exec_end_time = Column(TIME)                # 执行时间段结束时间-格林威治时间
    seller_push_time = Column(Integer)          # 跟卖数据上次发布时间
    seller_update_time = Column(Integer)        # 跟卖数据上次更新时间
    msku = Column(String(30))                   # msku
    timezone = Column(String(20))               # 时区
    is_site_time = Column(Integer)              # 是否是站点时间，0：不是站点时间（北京时间），1：站点时间
