import json
import copy
import logging
import traceback
from typing import Dict, Union

from fastapi import FastAPI, status
from fastapi.exceptions import RequestValidationError, HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse


from adjust_price_http import config
from adjust_price_http.logic import _insert, _update, _delete, _update_range
from adjust_price_http.models import Msg
from adjust_price_http.utils import send_msg_to_ding, create_session


logging.basicConfig(level=config.LOG_LEVEL, format='%(asctime)s - %(levelname)s - %(message)s - [%(name)s:%(lineno)s]')
app = FastAPI(title="adjust-price-http", debug=False if config.LOG_LEVEL.upper() == "DEBUG" else True)

operate_callback = {
    1: _insert,
    2: _update,
    3: _delete,
    4: _update_range
}


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError) -> JSONResponse:
    content = {
        "msg": "消息字段没有通过验证！",
        "fields": {},
        "original_message_content": exc.body
    }
    for e in exc.errors():
        content["fields"][e["loc"][-1]] = e['msg']
    await send_msg_to_ding(json.dumps(content, ensure_ascii=False))
    return JSONResponse(status_code=status.HTTP_200_OK, content={"code": 284, "msg": None})


@app.exception_handler(HTTPException)
async def normalize_msg(request: Request, exc: HTTPException) -> JSONResponse:
    msg = {"code": exc.status_code, "msg": exc.detail}
    await send_msg_to_ding(f"HTTPException！\n\n 错误信息：{msg}")
    return JSONResponse(status_code=status.HTTP_200_OK, content={"code": 284, "msg": None})


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):  # type: ignore
    response = JSONResponse(status_code=status.HTTP_200_OK, content={"code": 284, "msg": None})
    try:
        request.state.session = create_session()
        response = await call_next(request)
    finally:
        await request.state.session.close()
    return response


# URL是 PHP 基础服务设置的
@app.post("/workers/handle")
async def workers_handle(request: Request, msg: Msg) -> Union[Dict, JSONResponse]:
    origin_msg = msg.dict(exclude_unset=True)
    msg_list = copy.deepcopy(origin_msg["messageList"])
    for msg in msg_list:
        content = msg["messageContent"]
        function = operate_callback[content["operate_type"]]
        try:
            await function(request, content["data"])
        except Exception:
            # 捕获所有错误, 将其都抛出HTTPError, 统一给PHP返回 {"code": 284, "msg": None}; 防止服务内部错误, 导致消息丢失问题
            # 返回code = 284 PHP消息中心会在三天内重试, 直到成功!
            err = traceback.format_exc()
            raise HTTPException(status_code=status.HTTP_200_OK, detail=err)
    return {"code": status.HTTP_200_OK, "msg": None}


@app.get("")
async def get() -> str:
    return "OK"


if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8080)
