import logging
from typing import List

from fastapi.requests import Request
from sqlalchemy import select, insert, update

from adjust_price_http.models import TableFollowSale, TableCompetitive
from adjust_price_http.utils import verification_adjust_range

table_map = {
    1: TableFollowSale,
    2: TableCompetitive,
}


async def _insert(request: Request, data: List) -> None:
    """ 新增调价列表

    1. 完全新增, PHP发送的消息包含所有调价的信息
    2. 删除之后的调价列表, 重新恢复(is_delete的状态), 也是发送的新增消息, 咱们这边相当于修改其is_delete的状态

    所以这里需先查询db里面是否存在这条记录, 存在则修改is_delete的状态; 否则为完全新增
    """
    session = request.state.session
    for msg in data:
        origin_msg = msg.copy()
        rule_type = msg.pop('rule_type')
        msg = verification_adjust_range(rule_type, msg)
        table: TableFollowSale = table_map[rule_type]  # type: ignore

        res = await session.execute(select(table.id).where(
            table.company_id == msg["company_id"],
            table.asin == msg["asin"],
            table.sid == msg["sid"],
            table.msku == msg["msku"],
        ))
        schedule_id = res.first()
        if schedule_id:
            # 更新is_delete状态, 直接将php发过来的字段全更新
            await session.execute(update(table).where(table.id == schedule_id[0]).values(**msg))
        else:
            # 新增
            await session.execute(insert(table).values(**msg))
        await session.commit()
        flag = '修改is_delete' if schedule_id else '完全新增'
        logging.info(f"{table.__tablename__} 新增调价任务成功({flag}): {origin_msg}")


async def _update(request: Request, data: List) -> None:
    """ 更新调价列表

    """
    session = request.state.session
    for msg in data:
        origin_msg = msg.copy()

        rule_type = msg.pop('rule_type')
        msg = verification_adjust_range(rule_type, msg)
        table: TableFollowSale = table_map[rule_type]  # type: ignore
        await session.execute(update(table).where(
            table.company_id == msg["company_id"],
            table.asin == msg["asin"],
            table.sid == msg["sid"],
            table.msku == msg["msku"]
        ).values(**msg))
        await session.commit()
        logging.info(f"{table.__tablename__} 更新调价任务成功: {origin_msg}")


async def _delete(request: Request, data: List) -> None:
    """ 删除调价列表, 只需软删除

    """
    session = request.state.session
    for msg in data:
        origin_msg = msg.copy()

        rule_type = msg.pop('rule_type')
        table: TableFollowSale = table_map[rule_type]  # type: ignore
        await session.execute(update(table).where(
            table.company_id == msg["company_id"],
            table.asin == msg["asin"],
            table.sid == msg["sid"],
            table.msku == msg["msku"]
        ).values(is_delete=1))
        await session.commit()
        logging.info(f"{table.__tablename__} 删除调价任务成功: {origin_msg}")
    await session.close()


async def _update_range(request: Request, data: List) -> None:
    """ 修改竞品调价的 adjust_price_range
        将同一个company_id和rule_unique_id下所有的adjust_price_range都修改
    """
    session = request.state.session
    for msg in data:
        origin_msg = msg.copy()

        rule_type = msg.pop('rule_type')
        msg = verification_adjust_range(rule_type, msg)
        table: TableFollowSale = table_map[rule_type]  # type: ignore
        await session.execute(update(table).where(
            table.company_id == msg["company_id"],
            table.rule_unique_id == msg["rule_unique_id"],
        ).values(adjust_price_range=msg["adjust_price_range"]))
        await session.commit()
        logging.info(f"{table.__tablename__} 修改竞品调价的 adjust_price_range成功: {origin_msg}")
