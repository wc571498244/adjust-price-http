import os
from urllib.parse import quote_plus as urlquote

import yaml

parent_dir = os.path.dirname(os.path.dirname(__file__))
file_name = os.path.join(parent_dir, "config.yaml")

with open(file_name, "r") as fp:
    config = yaml.safe_load(fp)

user = config["db_config"]["user"]
password = config["db_config"]["password"]
host = config["db_config"]["host"]
port = config["db_config"]["port"]
db = config["db_config"]["db"]

DB_URL = f"mysql+aiomysql://{user}:%s@{host}:{port}/{db}" % urlquote(password)
LOG_LEVEL = config["log_level"]

DING_URL = config["ding_url"]
MOBILE = config["mobile"]
SECRET = config["secret"]
