import json
import time
import hmac
import hashlib
import logging
import base64
import urllib.parse
from typing import Tuple, Dict

import aiohttp
from fastapi import HTTPException
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from adjust_price_http import config

HEADERS = {
    "Content-Type": "application/json;charset=utf-8"
}


def get_sign() -> Tuple:
    timestamp = str(round(time.time() * 1000))
    secret = config.SECRET
    secret_enc = secret.encode('utf-8')
    string_to_sign = '{}\n{}'.format(timestamp, secret)
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
    sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
    return timestamp, sign


async def send_msg_to_ding(content: str) -> None:
    timestamp, sign = get_sign()
    url = config.DING_URL + f"&timestamp={timestamp}&sign={sign}"
    text = {
        "msgtype": "text",
        "text": {"content": content},
        "at": {
            "atMobiles": [
                config.MOBILE  # 如果需要@某人，这里写他的手机号
            ],
            "isAtAll": 0  # 如果需要@所有人，这里写1
        }
    }
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, timeout=5, data=json.dumps(text), headers=HEADERS) as rsp:
            msg = await rsp.text()
            if rsp.status == 200:
                logging.info("预警信息已推送到钉钉！{}".format(msg))
            else:
                logging.error(f"推送失败，钉钉返回状态码：{rsp.status}, 钉钉返回的信息：{msg}")


def create_session() -> AsyncSession:
    async_engine = create_async_engine(config.DB_URL, poolclass=NullPool)
    async_session = sessionmaker(async_engine, expire_on_commit=False, class_=AsyncSession)
    return async_session()


def verification_adjust_range(rule_type: int, msg: Dict) -> Dict:
    if rule_type == 2 and "adjust_price_range" not in msg:
        raise HTTPException(
            status_code=284, detail="rule_type=2, 必须上传adjust_price_range字段"
        )
    elif rule_type == 1 and "adjust_price_range" in msg:
        raise HTTPException(
            status_code=284, detail="rule_type=1, 不能传adjust_price_range字段!"
        )
    elif "adjust_price_range" in msg:
        msg["adjust_price_range"] = json.dumps(msg["adjust_price_range"])
    return msg
