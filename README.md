# adjust-price-http


```json
{
	"messageId": "42da3b49c5f14e95b220793d611d59bcgnaf9d20210609",
	"messageRouterKey": "http:\/\/10.49.0.34:6091\/test|90124993550340096",
	"messageTypeCode": "mws_adjust_price_inform_message",
	"messageContent": {
		"company_id": 90124993550340096,
		"operate_type": 1|2|3|41新增2修改3删除4竞品规则range修改adjust_price_range"data": [
        {
			//新增
            "company_id": 90124993550340096,
			"asin": "B078RJ1KZ6",
			"msku": 'dfeyfde',
			"sid": 1,
			"rule_type": 1,
			"mid": 1,
			"seller_id": "A303OEQ77COZA8",
			"rule_unique_id": 49880070075973611,
			"site_url": "https://www.amazon.com",
			"status": 3,
			"is_delete": 0,
			"exec_start_gmt_time": "02:18:59",
			"exec_end_gmt_time": "20:18:59",
			"zid": 2,
			"adjust_price_range": []
		},
		{
			//修改"company_id": 90124993550340096,
			"asin": "B078RJ1KZ6",
			"msku": 'dfeyfde',
			"sid": 1,
			"rule_type": 1,
			//需要修改的字段"exec_start_gmt_time": "02:18:59",
			"exec_end_gmt_time": "20:18:59",
			
		},
		{
			//删除"company_id": 90124993550340096,
			"asin": "B078RJ1KZ6",
			"msku": 'dfeyfde',
			"sid": 1,
			"rule_type": 1,
			
		},
		{
			//竞品规则range修改adjust_price_range"company_id": 90124993550340096,
			"rule_unique_id": "49880070075973611",
			"adjust_price_range": [],
			"rule_type": 2,
			
		}]
	},
	"createTime": 1623244325049,
	"mqInfo": {
		"topic": "mws_adjust_price",
		"partition": 0,
		"offset": 697
	},
	"mark": "ZQsL8gHt7VaL9wq8OWxq3DsN3F4m97b6ezNovpUgLYlpVlcXJ+hX84IHR773smYEuco9lnX+Tki44hXSMmpyYGNj22bQ5EiA3l5CHF7BUqpxYg59oVZNoyCDqZ8+CKHguvwZ6HzIWzaQx\/s99Lkeug=="
}

```