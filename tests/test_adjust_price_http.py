#!/usr/bin/python3
# encoding: utf-8

"""
test_adjust_price_http
----------------------------------

Tests for `adjust_price_http` module.
"""
from copy import deepcopy

from fastapi.testclient import TestClient

from adjust_price_http.main import app


class TestAdjustPriceHttp:
    base_url = "http://localhost:8080/"

    base_msg = {
        "messageList": [
            {
                "messageContent": {
                    "company_id": 0,
                    "operate_type": 0,  # 1新增2修改3删除4竞品规则range修改adjust_price_range
                    "data": []
                },
            }
        ]
    }
    client = None

    @classmethod
    def setup_class(cls):
        cls.client = TestClient(app)

    def test_insert_adjust_price(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 123456789
        msg["messageList"][0]["messageContent"]["operate_type"] = 1
        msg["messageList"][0]["messageContent"]["data"] = [{
            "company_id": 123456789,
            "asin": "B078RJ1KZ6",
            "msku": "abcd",
            "sid": 1,
            "rule_type": 1,
            "mid": 1,
            "seller_id": "A303OEQ77COZA8",
            "rule_unique_id": 49880070075973611,
            "site_url": "https://www.amazon.com",
            "status": 3,
            "is_delete": 0,
            "exec_start_time": "02:18:59",
            "exec_end_time": "21:18:59",
            "zid": 2,
            "timezone": "Asia/Shanghai",
            "is_site_time": 0,
        }]

        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_insert_competitive(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 987654321
        msg["messageList"][0]["messageContent"]["operate_type"] = 1
        msg["messageList"][0]["messageContent"]["data"] = [{
            "company_id": 987654321,
            "asin": "B09335DXBV",
            "msku": "BN-Q83M-S7I3",
            "sid": 1,
            "rule_type": 2,
            "mid": 1,
            "seller_id": "A303OEQ77COZA8",
            "rule_unique_id": 49880070075973611,
            "site_url": "https://www.amazon.com",
            "status": 3,
            "is_delete": 0,
            "exec_start_time": "02:18:59",
            "exec_end_time": "21:18:59",
            "zid": 2,
            "timezone": "Asia/Shanghai",
            "is_site_time": 0,
            "adjust_price_range": ["B0784BRQ5Z", "B07GZHJNLG", "B07D72L983"]
        }]

        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_update_adjust_price(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 123456789
        msg["messageList"][0]["messageContent"]["operate_type"] = 2
        msg["messageList"][0]["messageContent"]["data"] = [{
            "company_id": 123456789,
            "asin": "B078RJ1KZ6",
            "msku": "abcd",
            "sid": 1,
            "rule_type": 1,
            "mid": 1,
            "seller_id": "A303OEQ77COZA8",
            "rule_unique_id": 49880070075973611,
            "site_url": "https://www.amazon.com",
            "status": 3,
            "is_delete": 0,
            "exec_start_time": "00:18:59",
            "exec_end_time": "23:18:59",
            "zid": 2,
            "timezone": "Europe/Paris",
            "is_site_time": 1,
        }]
        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_update_competitive(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 987654321
        msg["messageList"][0]["messageContent"]["operate_type"] = 2
        msg["messageList"][0]["messageContent"]["data"] = [{
            "company_id": 987654321,
            "asin": "B09335DXBV",
            "msku": "BN-Q83M-S7I3",
            "sid": 1,
            "rule_type": 2,
            "mid": 1,
            "seller_id": "A303OEQ77COZA8",
            "rule_unique_id": 49880070075973611,
            "site_url": "https://www.amazon.com",
            "status": 3,
            "is_delete": 0,
            "exec_start_time": "00:18:59",
            "exec_end_time": "23:18:59",
            "zid": 2,
            "timezone": "Europe/Paris",
            "is_site_time": 1,
            "adjust_price_range": ["B0784BRQ5Z"]
        }]
        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_delete_adjust_price(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 123456789
        msg["messageList"][0]["messageContent"]["operate_type"] = 3
        msg["messageList"][0]["messageContent"]["data"] = [{
            "rule_type": 1,
            "company_id": 123456789,
            "asin": "B078RJ1KZ6",
            "msku": "abcd",
            "sid": 1,
            "mid": 1,
        }]
        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_delete_competitive(self):
        msg = deepcopy(self.base_msg)
        msg["messageList"][0]["messageContent"]["company_id"] = 987654321
        msg["messageList"][0]["messageContent"]["operate_type"] = 3
        msg["messageList"][0]["messageContent"]["data"] = [{
            "rule_type": 2,
            "company_id": 987654321,
            "asin": "B09335DXBV1",
            "msku": "BN-Q83M-S7I3",
            "sid": 1,
            "mid": 1,
        }]
        rsp = self.client.post(self.base_url + "workers/handle", json=msg)
        assert rsp.status_code == 200
        res = rsp.json()
        assert res == {"code": 200, "msg": None}

    def test_body_error(self):
        rsp = self.client.post(self.base_url + "workers/handle", json=self.base_msg)
        assert rsp.status_code == 200
