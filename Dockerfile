FROM python:3.6.5
ENV TZ=Asia/Shanghai
ARG PKG

WORKDIR /tmp
COPY requirements.txt /tmp
RUN pip3 install -i https://pypi.douban.com/simple -r /tmp/requirements.txt
COPY ./ /tmp

EXPOSE 8080
CMD ["uvicorn", "adjust_price_http.main:app", "--host", "0.0.0.0", "--port", "8080", "--workers", "4"]
